---
layout: post
title: Elixir 1.6.3 Supports Comments in the middle of pipelines, when and | expressions
subtitle: The new updated feature in Elixir 1.6.3
kicker:
author: blackode
author-twitter: blackode
tags: [elixir, tips, programming]
images: /assets/images/blog/2018/Elixir-Supports-Comments-in-the-middle-of-pipelines
summary: >-
 The new updated feature in Elixir 1.6.3

published: true
---
This article is all about saying that Elixir 1.6.3 supports comments in middle of `|>` , `when`, `|` still you do `mix format` .


### a. Comments in the middle of pipelines

I assume that you are in the **Elixir** version of **<1.6.3**

Create a file `comment.`ex and add the following code inside the file


```elixir
defmodule Comment do
  def square(number), do: number * number
  def cube(number), do: number * number * number


  def run(number) do
    number
    # this is square function
    |> square
    # this is cube function
    |> cube
    # this is used for inspecting the results
    |> IO.inspect()
  end
end
```

After adding the content to the file run the following command


```elixir
$ mix format comment.ex
```

I hope that you are in the same directory of the file `comment.ex` . The formater converts the above code into the following format.


```elixir
defmodule Comment do
  def square(number), do: number * number
  def cube(number), do: number * number * number

  def run(number) do
    # this is square function
    # this is cube function
    # this is used for inspecting the results
    number
    |> square
    |> cube
    |> IO.inspect()
  end
end
```

Did you observe that all the comments are moved to top of definition. But this is updated in `Elixir v1.6.3` . The comments are placed in the middle of pipelines in the `Elixir v1.6.3`


{% include blog_image.html image= "Elixir v1.6.1 mix format comment.ex screenshot.png"%}


Now install **Elixir V1.6.3** and try again. It still supports that comments in the middle of pipelines.


### b. comments in the middle of | expressions


```elixir
@spec square integer()::
                    :ok
                    | :invalid
                    # :unknown
                    | :other
```


### c. comments in the middle of when

Similarly, we can comment in the middle of `when` guards like in the following

Here, inside the file, I added another function `or_gate_truth/2` for showing the demo.


```elixir
#comment.ex

defmodule Comment do  

  ...  

def or_gate_truth(x, y)
    #checking x is 1
    when x == 1
    #checking y is 1
    when y ==1 do
    True
  end

end
```


However, if you format the file `mix format comment.ex` , the formatter still collects all the comments to top of the function like in the following way in **ELixir < 1.6.3**


```elixir
defmodule Comment do
  ...  

  # checking x is 1
  # cheking y is 1
  def or_gate_truth(x, y)
      when x == 1
      when y == 1 do
    True
  end
end
```


{% include blog_image.html image= "1_.png"%}


But, It is updated in `Elixir v1.6.3` it still places the comments in the middle of the **when** expressions.

Give a try by commenting in the middle of pipelines, `when`, `|` union expressions. Install the `Elixir V1.6.3` before giving a try
