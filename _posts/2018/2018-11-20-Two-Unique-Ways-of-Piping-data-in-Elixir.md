---
layout: post
title: Two Unique Ways of Piping data in Elixir
subtitle: Dreaming in pipe |>
kicker:
author: blackode
author-twitter: blackode
tags: [elixir, tips, programming]
images: /assets/images/blog/2018/Two-Unique-Ways-of-Piping-data-in-Elixir
summary: >-
 No junk, straight to code

published: true
---

No junk, straight to code

Assume `data = {:ok, [1,2,3,4,5]}` and we need to pass the `data` to the next function.

**Do we need to write another function to do that job for this small operation?** I don’t think so.

Check this…

### TYPE –1 🎉

```elixir
data
|> (fn {:ok, list} -> list  end).()
|> length()
5
```
{% include blog_image.html image= "1_.png"%}

### TYPE — 2 🎉


Here, instead of sending the `length` itself, I would like to send a message something looks like `Total Images : 5` **Do I again need to use inline function fn again here?** I don’t think so.

Check this…

```elixir
data
|> (& {:ok, list} -> list ).()
|> length()
|> (&"Total Images : #{&1}").()
```

{% include blog_image.html image= "2_.png"%}

Hope you like them. Thanks for reading…
