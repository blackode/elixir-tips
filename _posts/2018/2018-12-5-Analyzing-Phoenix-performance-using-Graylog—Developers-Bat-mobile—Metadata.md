---
layout: post
title: Analyzing Phoenix performance using Graylog — Developers Bat mobile — Metadata.
subtitle:
kicker:
author: anwesh
author-twitter: anwesh
tags: [elixir, tips, programming]
images: /assets/images/blog/2018/Analyzing-Phoenix-performance-using-Graylog
summary: >-
 Data may be a new Oil but Metadata is jet fuel.
published: true
---
 **Data** may be a new **Oil** but Metadata is **jet** **fuel**.



Logging is a modern standard practice for any application. The analysis is a real deal about it.

Let’s have a small glimpse of analytic on Elixir and why it is important to the developer.

Of course, Metadata is a real deal, unless it is tweaked to meet the needs of a developer and to make a concrete decision. My recommendation would be the different stages of development to keep an eye on the application performance.


### What we made with metadata?

1. Build a custom built dashboard, unlike appsignal or rollbar.
1. Monitor Performance and evaluate hardware of hosting provider.


Let’s start with **Graylog2**.


Please don’t start about **ELK vs Graylog**. Yes, We tested **ELK + Grafana**, we found the **Graylog** has things, out of the box like **UDP** , & plugging works like a charm. Moreover, both are based on **elasticsearch**.

If you are still into **ELK**, **bleachreport** posted an  [**article**](https://dev.bleacherreport.com/elixir-phoenix-centralized-http-logging-aa50efe3105b)
.

**Current** **Intention** to index both echo and phoenix log’s

### GELF Logger Backend

— Configuration is straight forward

https://github.com/jschniper/gelf_logger#configuration

Sample configuration for **OVH** ([Config](https://medium.com/blackode/logging-elixir-application-on-graylog-on-ovh-or-similar-cloud-providers-55f75122521c)
)

Now we have to send log in `json` so, **graylog** can analysis it.

**BleachReport** has built two plugins for this specific purpose.

1. https://hex.pm/packages/plug_logger_json
2. https://hex.pm/packages/ecto_logger_json

Both work seamlessly, if you feel a need for more data to be logged, first tweak the plugin, second send the data to **garylog** and catch it there. (Second option is easy to implement).


### Graylog Application

Build a node for incoming data at **graylog**.

Configure the **host** and **port** and make sure it is allowed in your firewall.


{% include blog_image.html image= "Graylog stream.png"%}


Now, analysis the incoming data, **Graylog** has **JSON** parser.

**Phoenix**

{% include blog_image.html image= "1_.png"%}

**Ecto**

{% include blog_image.html image= "2_.png"%}
{% include blog_image.html image= "3_.png"%}


Once the data is indexed, its up to the developers vision and their crazy/creative way to use the dashboard.

We currently evaluating **CleverCloud** provider, a SAAS for elixir hosting vs dedicated hosting at **Scaleway**. Both are hosted at Online.net. Metadata came to our resume to settle the dispute.

We are blind at CPU capacity and RAM performance at **clevercloud**, we wanted to get some metrics to evaluate it.


{% include blog_image.html image= "Gralog dashboard of comparission of Clevercloud and Scaleway.png"%}


Valla, We are collecting metrics, and populate things accordingly.
