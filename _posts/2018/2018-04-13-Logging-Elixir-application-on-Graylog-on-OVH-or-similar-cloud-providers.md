---
layout: post
title: Logging Elixir application on Graylog on OVH or similar cloud providers
subtitle:
kicker:
author: anwesh
author-twitter: anwesh
tags: [elixir, tips, programming]
images: /assets/images/blog/2018/Logging-Elixir-application-on-Graylog-on-OVH-or-similar-cloud-providers
summary: >-
 A logger backend that will generate Graylog Extended Log Format messages.
published: true
---
A logger backend that will generate Graylog Extended Log Format messages.


Taking advatage of Elixir logger API, [gelf_logger](https://hex.pm/packages/gelf_logger)

* **Add** `gelf_logger` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:gelf_logger, "~> 0.7.3"}]
end
```

* **Ensure** `logger` and `gelf_logger` is started before your application (pre Elixir 1.4):

```elixir
def application do
  [extra_applications: [:logger]]
end
```

### Configuration

* In your `config.exs` (or in your `#{Mix.env}.exs`-files):


```elixir
config :logger,
  backends: [:console, {Logger.Backends.Gelf, :gelf_logger}]

  config :logger, :gelf_logger,
      port: 2202,
      application: "aadya",
      compression: :raw,
      metadata: [:request_id, :function, :module, :file, :line],
      host: "<server_host>.logs.ovh.com",
      tags: [
         "X-OVH-TOKEN": "xxxxxxxx-x-x-x-xxx-xxx"
      ]
```

### Output


**Success**

{% include blog_image.html image= "1_.png"%}


{% include blog_image.html image= "2_.png"%}

**Error**

{% include blog_image.html image= "3_.png"%}
