---
layout: post
title: A Beautiful Love between Elixir & Programmer — love-letter.
subtitle: To love with elixir feelings.
kicker:
author: blackode
author-twitter: blackode
tags: [elixir, tips, programming]
images: /assets/images/blog/2018/A-Beautiful-Love-between-Elixir-&-Programmer-love-letter
summary: >-
 To love with elixir feelings.

published: true
---
Dear sweetest **Elixir** — writing with ❤️,

I saw you very first time in a conference with your farther (creator) [José Valim](https://twitter.com/josevalim)
. Seeing you directly into your purple eyes, my heart felt like a **mountain** **of** **snow**, started melting `bit` by `bit` and eventually turned melting `byte` by `byte`. I tried to control it from melting down, I didn’t and I discerned, I have been `spawn_link` with you.

## Your meeting is the best day of my life.


{% include blog_image.html image= "1_.png"%}

The moment I saw the **shape** **of** **you** `syntax` , I didn’t take off my eyes from you. I stared at you, not failing a second, with a **shower** **of** **love** **and** **shine**. My pal, sitting next to me, interrupted me from looking into your eyes. I just turned to him and `restarted` looking at​ you `simple one for one.`

I feel like **kissing** 😽 by with my hand around your waist. I feel like walking in a shower of love holding your hand, staying together and looking into your eyes without a blink. I feel your presence in me whenever I feel to code.


{% include blog_image.html image= "2_.png"%}


With your enormous love ❤️, turned me into an `immutable` living creature, bringing the fame and name as a person of programming with your outstanding `list` of features and awesome functionalities. I defined a `structure` to my style of coding `map`ping with your `pattern` of love.

You look fresh and pure on every day. You will surprise me every time you come up with a new `update`. I adopt you like a child. You are the greatest monument ever built in the history of coding.


## You are simple, smart and sweet ❤.

{% include blog_image.html image= "3_.png"%}


You have been my best moment from the moment I saw 😍 and looked into your lovable purple eyes. You are so kind with your `GenSever` heart of helping in parallel. I started thinking out of the box after our first meet.

I started following your footsteps like a shadow in the light. You are my candle in the darkness of errors. Unknowingly, you happen to be my beautiful mentor and letting me dwell into your lovable heart ❤️. Since then, you have been my `map` to travel deep inside the universe and have been the node in my life to start the fresh coding life again.

You came and changed everything in me, on me, and of me. Now, being apart from you, can’t even give a thought to.


## You happen to be my first thought when we are far from each other.


{% include blog_image.html image= "4_.png"%}


I feel the presence of freedom, joy, and happiness when I keep coding with you. I love the way you let me to turn you into the shape I like meta-programming . I never ever thought I’d love ❤️ you this much in coding and I never planned to be with you this often.

Looking into the night sky with thousands of stars ⭐️ ⭐️ ️ ⭐️ ⭐️️, I remember your million processes, because they sparkle in performance exactly like stars ⭐️ ⭐️ ⭐️. When I see the sun ☀️ I remember you, because you brighten not just my day, but my entire coding life.


## I like to travel with you in a lifeboat in deep in the middle in the ocean in alone forgetting the world.


{% include blog_image.html image= "5_.png"%}


Sometimes, observing your beauty in coding `syntax` makes me absolutely speechless. Words fail me. I love you so much that I’m going crazy with you. I can’t stop thinking and dreaming about you. Only it makes me feel coding alive. Coding would be such a dark place without your presence.

The only real thing in this world is time, you saved a lot of mine with your short and smart way of coding style — **pattern matching.**

Coding with you is unexplained feeling. It’s eternal and enormous that doesn’t fit in any words, especially these three simple words “**I love you**”.

— Yours lovingly


❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️

##   An Elixir Programmer —

❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️


##     My feeling 😃 after writing this —

##    Coding in Elixir is much easier than writing a love letter to propose Elixir.


Long live **Elixir**. Dedicated to all programmers in love ❤️ with **Elixir** and to the Creator [José Valim ](https://twitter.com/josevalim)
 for an outstanding language.

Share your feelings as an Elixir Developer.
