---
layout: post
title: Elixir as Wife
subtitle:
kicker:
author: blackode
author-twitter: blackode
tags: [elixir, tips, programming]
images: /assets/images/blog/2019/Elixir-as-Wife
summary: >-
  Just imagine the consequences…

published: true
---
Just imagine the consequences…

Well, ladies & gentlemen presenting you **Elixir**, the adorable **WIFE**.


**Do you love your wife?** Of course you can’t answer me in face as you can’t see me
while reading this. Just look at your heart and just think. If you are thinking
then you are not loving. F\*\*k man don’t think, It’s not a question about the
science or astrology to think and answer. I know it is still hard for you to say “ **YES** ” .

It’s a joke you can laugh freely as I can’t charge you for this.

Any way, who else loves the **wife in life uses knife to cut you in half**. Man this
is an insane rhythm of saying as wife, life and knife. Do you know why they sound
alike ? It’s cos they follow similar protocol of being good and bad transformations
with out any emotion.

My wife, <code>ELIXIR</code> I call her with another name for now I forgot to tell you. It’s almost
3 years passed after our marriage. I’d like to share how does it feel when you marry
a girl who possessed completely by <code>ELIXIR</code>.

It’s a fictional. Again I am saying it’s a fictional story. If anyone either
directly or indirectly related to this, man you are lucky take this as granted.


### #Supervisor


{% include blog_image.html image= "1_.jpg"%}

One fine morning, it’d been raining whole night. The weather was so romantic.
The cool breezes are very tempting.

So I asked her, “ **Let’s have children** ”.

She said, " **The Supervisor is not starting our children processes**".

What? Ok. Then I asked, " **why?**".

She replied turning towards me spreading her hands wide in anger, " **It's cos of f\*\*king run time errors in our relationship**".

After then, I took a step back from her, turned back and escaped silently, unnoticed.

To silent her anger, I told that I am going to buy her new jewellery and
matching dress. At any cost, I'm not going to do it. It’s a big lie and life
savior for any husband.

### #Gen-Server

{% include blog_image.html image= "2_.jpg"%}

After solving our run time problems in our running relationship, we went out on
a vacation to a place where you find all the people beautiful except your wife and
husband as you already saw them half naked, the Beach. It’s sandy and hot outside.

While we are walking along the beach, holding her hand, she asked me why I always
send late reply to her message. I said, “ **I was busy in taking other calls** ”. Then,
she advised me to assign multiple workers to handle info from external messages.
So, now I can handle calls from home(Wife Module) by casting her synchronously and asynchronously.

On the way to my home from office, I met with an incident. A stranger came to me
and asked me to deliver a message to my wife that he is going to die(exit process),
and not to re spawn him again. I didn't understand what he is talking about, called
my wife and explained about the incident.

Then she said, " **I made myself distributed. So, external processes are trying to link with me**”
I said, “ **What the hell?** ”. I cannot do more that. She exposed her process identifier, **PID** and tried
to explain me about the general concurrent process to conquer me.

### #Behavior

{% include blog_image.html image= "3_.jpg"%}

I took almost 2 weeks to come out from the shock she gave with her distributed nature.
From then I kept a gate keeper to not let anyone with out a secret cookie. When
everything is fine she told me that her friend is coming home. I let her to invite.

To impress her friend, I put on new dress and added scent to appear myself special. **Men will be Men**.
Me and my wife were waiting at the gate to welcome her friend. I’m more excited.
A car came and stopped right in front us. I am adjusting my Tie and coat to give grant welcome.

The waiting friend had finally revealed and to my surprise he is a **MAN** who is better
than me in all possible angles. I can’t digest it. I except a young and beautiful
girl at least a pretty good looking lady. Now, I am in a sarcastic mood. Anyway, who
wishes a man like macho who is better than him as her wife’s friend. That’s a tragedy.
Right now he is with my wife. They were laughing and I am crying inside.


He is almost behaving like her own husband. A volcano is running in my heart.
No more controlling. So I asked about his weird behavior then my wife told me that
he is using husband module and overriding all my defined functions. `use Husband`,
A behavioral module. I said, “ **What heck is all that ?** ”. She told, “ **It's not a heck.
It's your behavior and rights were shared** ”.

F\*\*k man every time she comes with a
new concept. From then instead of understanding her, I started following her.

We eventually ended up each other being at different **NODEs** with different cookies.
