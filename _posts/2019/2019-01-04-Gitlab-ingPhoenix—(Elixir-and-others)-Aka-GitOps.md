---
layout: post
title: Gitlab-ing Phoenix — (Elixir and others) Aka GitOps
subtitle:
kicker:
author: blackode
author-twitter: blackode
tags: [elixir, tips, programming]
images: /assets/images/blog/2019/Gitlab-ingPhoenix
summary: >-
 This article is all about Automating Phoenix Development & Deployment using Gitlab CI/CD.

published: true
---
This article is all about **Automating Phoenix Development & Deployment** using **Gitlab CI/CD.**

As we all know just creating the things is not enough in the current world of software development as nothing is permanent. New things are always strange to the people using the software so, we need to document them in the first case. Things always change and we need to accept them. Things always grow and we need to scale them too. Eventually, we need to automate and optimize the things.

So, to improve the efficiency and effectiveness, the **Creation** should followed by **Documentation** followed by **Automation** followed by **Optimization**.



 {% include blog_image.html image= "1_.png"%}.



### What is GitLab ?

[Gitlab](https://gitlab.com/)
 is a fascinating software started as a code collaboration. Now, it’s evolved into an end to end sophisticated software development tool. Here at **Ahamtech**, we have been using the Gitlab as a software development tool since 6 years.

Many reasons to adore(I ❤️ it) **Gitlab**. Of course there are other greater tools like **GitHub**, **Jenkins**, **Travis**, **CircleCI** and others as well.

According to my experience, none of the providers or solutions match to **Gitlab** to control your infrastructure. Like every software has flaws, **Gitlab** is no exception. It is built on top of **Ruby**, feels kind of bit slow in few aspects. But, it weighs the benefits for us. Enough about GitLab pampering.

Elixir is a major software ecosystem in our company, relied to build many unique and efficient software till date. Our team enjoys writing blogs about elixir and spreading community.


Pairing up **Elixir** and **Gitlab** for development gives you much comfort in coding. Currently, I am focusing on **Phoenix** testing and deployment using `edeliver`.

Erlang has an inbuilt release manager, which takes **near** **zero** downtime for code upgrading during run-time.


### What are we going to do here?


* Phoenix Auto Deployment using — Distillery and Edeliver

* Gitlab CI/CD for testing

* Gitlab CI/CD for docs inside project wiki.


Development Pipelines (DI) are really cool in **Gitlab CI/CD**.
I suggest you to check articles on how Gitlab pipelines are working and how to use them. I’m not focusing on those topics is not a part of this article.

At an initial step, we will focus on **automated testing**.
There is great tutorial by Gitlab team on testing [“Phoenix app using Gitlab CI”](https://docs.gitlab.com/ee/ci/example/test_phoenix_app_with_gitlab_ci_cd/)

**Sample Test CI/CD:**


```elixir
image: elixir:latest

services:
  - postgres:latest

variables:
  POSTGRES_DB: test_test
  POSTGRES_HOST: postgres
  POSTGRES_USER: postgres
  POSTGRES_PASSWORD: "postgres"
  MIX_ENV: "test"

before_script:
  - apt-get update && apt-get -y install postgresql-client
  - mix deps.get
  - mix ecto.create
  - mix ecto.migrate

mix:
  script:
  - mix test
```


We have a thumb rule here. **Never** merge any branch in to **develop** when test cases are failed.

Once test pipeline is passed, we have multiple things to do.

* Delivering the build to Production.
* Make new docs and upload static server/object storage.

**First step — Deployment.**

**Edeliver — Auto Deployment:**



Now, once develop job is triggered, the auto deployment is triggered for that specific commit.


{% include blog_image.html image= "2_.png"%}


With the above approach, we have upgraded remote using auto deployment.


**Next comes is updating docs in side GitLab wiki.**

There are two ways of doing it.


* Gitlab Pages
* Wiki in side project.

Gitlab pages are really simple. Just enable artifacts for docs folder and update pages config inside your project which is the super fast solution. But, those docs are visible to public. If you have any project for public, it’s the one of the solutions.

But, most of the project builds are private(like us). We wish to save in wiki and only people can access to docs.

Now here comes the challenge. **Gitlab** Wiki only supports **Markdown** format.

So, **MIX** straight forward doesn’t support markdown, but internally **MIX** uses markdown format to convert to **HTML**.

We wrote a small patch of work to build **MarkDown** files (which is far from production).

[**ex_docs_md**](https://gitlab.com/ahamtech/elixir/ex_docs_md)



{% include tip.html tip= "mix docs -f md"%}


This will generate, Markdown files.


```elixir
docs-md:
  stage: deploy
  allow_failure: true
  script:
    - git clone {repo} doc
    - rm -rf doc/*
    - mix deps.get
    - mix docs -f md
    - cd doc
    - git config --global user.email "ops@ahamtech.in"
    - git config --global user.name "Gitlab Runner"
    - git add .
    - git commit -am "$CI_COMMIT_SHA"
    - git push -u origin master
  only:
    - docs
```


{% include blog_image.html image= "3_.png"%}


Our **Testing, Deployment, and Documentation** is achieved using **Gitlab CI/CD**. In future, we might get inspired with some other work flow and we are happy to share with you.

**Hope you liked it.**
