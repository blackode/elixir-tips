---
layout: post
title: The Secret behind Elixir Operator Re-Definitions + to -              
subtitle: Just for Fun
kicker: Develop and Explore
author: blackode
author-twitter: blackode
tags: [elixir, tips, programming]
images: /assets/images/blog/2017/The-Secret-behind-Elixir-Operator-Re-Definitions
summary: >-
   The universal meaning of + is to add but for a change, we are making this as subtraction.
published: true
---
**Elixir 1.5.2**

Hello, everyone. Today, we are going to do some crazy stuff. This article is just for fun. The only intention is to show you this can possible in elixir.

The universal meaning of + is to add but for a change, we are making this as subtraction.

### How to achieve this ?

We can use def* constructs (`def, defp, defmacro`, …) for re-defining the operators in elixir.

### The Only 1 Rule

The only rule is the name of definition. It should be same as how we use the operator while coding. In general, we use `+` as `a + b` So, our definition name should be same as **a + b**.

Check the following code for re-defining **+** to **-**

```elixir
defmodule MyWrongOperators do

    def a + b do
    a - b
  end
end
```

Nothing is new here. We have just created a function or definition but with a **little tricky name**.

**Boom!!** You have turned the world upside down. We are doing things as they are possible and elixir allowed us to do. But these are nothing to do.

### How to use?

Here, in elixir, there exists a power that pulls you back from using even though it is allowed you to create with out any hurdles.

If you force to use the module like in regular style of coding, it hits you with an exception. Lets check that.

```elixir
iex> import MyWrongOperators

iex> 1 + 2

** ** (CompileError) iex:4: function +/2 imported from both MyWrongOperators and Kernel, call is ambiguous


```
 {% include blog_image.html image= "Compile Error When Using the Module Directly.png"%}

 Well, what is the power throwing you this error by not allowing? Can you think for a while and take a guess?

I don’t want to linger you anymore. The `Kernel` module loaded by default. This `Kernel` module contains a +/2 function which is also loaded. So, when we are importing our module `MyWrongOperators` , we loaded `+/2` function again. Now with in our scope, there exist two `+/2` definitions. So, the compiler got confused here.

### How to bypass this ?

Well, we need all the functions in `Kernel` module except the `+/2`. This can be achieved with `except` while loading a module.

```elixir
import Kernel, except: [+: 2]
import MyWrongOperators
```

The above LOC clearly tells not to load the `+/2` function. So, we only have one `+/2` definition from the module `MyWrongOperators` . So, the compiler can now use `+/2` definition with out getting confused.

```elixir
iex > import Kernel, except: [+: 2]
Kernel

iex> import MyWrongOperators
MyWrongOperatorsiex> 1 + 2
-1            # 1 - 2 operation performed here as we defined.
```

{% include blog_image.html image= "The perfect Usage.png"%}

In the above screen shot, at `iex(3)` line we are loading the `Kernel` module with out letting it to load `+/2` definition. So, it worked perfectly.

We successfully executed the wrong operations.

### Things to Remember
Q. Can I create new operator ?
Ans. No. But, Elixir is capable of parsing a predefined set of operators like

*    `|`

*    `|||`

*    `&&&`

*    `<<<`

*    `>>>`

*    `<<~`

*    `~>`

*    `<~`

*    `<~>`

*    `<|>`

*    `^^^`

*    `~~~`


Q. Can I Override every other operators ?
Ans. Yes. You can. But, while using don’t forget to import Kernel module with those override operators as exceptional.

Q. Is it good to override operators ?
Ans. Of course it not good and not recommended.

Q. Why it is not recommended ?
Ans. Custom-defined operators can be really hard to read and even more to understand, as they don’t have a descriptive name like functions do.

### Live Demo

<script id="asciicast-151933" src="https://asciinema.org/a/151933.js" async></script>

{% include warning.html title= "Warning ~~ " warning= "Highly not recommended to do…"%}
