---
layout: default
title: About
---
<section>
<!-- Header -->
<header class="bg-light text-center py-5 mb-4 mt-4">
  <div class="container">
    <h1 class="font-weight-light text-dark">Meet the Team</h1>
	<p class="text-black-50"> We are all very different. We were born in different cities, 
	at different times, we love different music, food, movies. 
	But we have something that unites us all. It is our thoughts. 
	We are not just a team, we are a family. 
	</p>
  </div>
</header>

<!-- Page Content -->
<div class="py-5">
  <div class="col-sm-12 row col-md-8 offset-md-2">
  {% for profile in site.data.profiles %}
    <!-- Team Member 1 -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
         <a href="https://twitter.com/{{profile.twitter}}">
	  {% if profile.image == "" %}
	  <img src="/assets/images/default.png" class="card-img-top" alt="...">
	  {%else%}
	  <img src="/assets/images/authors/{{profile.codename}}/{{profile.image}}" class="card-img-top" alt="...">
	  {%endif%}
	  </a>
        <div class="card-body text-center">
		<h5 class="card-title mb-0">{{profile.role}}</h5>
		<div class="card-text text-black-50 text-bold">
			{{profile.name}} ({{ profile.codename }})
		</div>
        <div> 
		<small class="text-black-50"> "{{profile.bio}}"</small>
        </div>
		</div>
      </div>
    </div>
{% endfor %}
  </div>
  <!-- /.row -->

</div>
<!-- /.container -->
</section>

<section class="mt-3 mb-4">
<div class="text-center text-bold"> Just Keep Smiling :) </div>
<div class="text-center">__________x__________ </div>
</section>


