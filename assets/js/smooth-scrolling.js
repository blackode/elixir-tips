$('a[href*="#"]')
  .not('[href="#0"]')
  .on('click', function(event) 
{
  var target = $(this.hash);
  event.preventDefault();
  $('html, body').animate({
    scrollTop: target.offset().top 
  }, 500);
}
);

$('.nav-item').on('click', function () {
  $(this).siblings().removeClass('active');
  $(this).addClass('active');
})

